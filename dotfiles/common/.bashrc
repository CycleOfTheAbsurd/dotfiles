#
# ~/.bashrc
#

# If not running interactively, don't do anything
[[ $- != *i* ]] && return

#Colors?  Used for the prompt.
#Regular text color
BLACK='\[\e[0;30m\]'
#Bold text color
BBLACK='\[\e[1;30m\]'
#background color
BGBLACK='\[\e[40m\]'
RED='\[\e[0;31m\]'
BRED='\[\e[1;31m\]'
BGRED='\[\e[41m\]'
GREEN='\[\e[0;32m\]'
BGREEN='\[\e[1;32m\]'
BGGREEN='\[\e[1;32m\]'
YELLOW='\[\e[0;33m\]'
BYELLOW='\[\e[1;33m\]'
BGYELLOW='\[\e[1;33m\]'
BLUE='\[\e[0;34m\]'
BBLUE='\[\e[1;34m\]'
BGBLUE='\[\e[1;34m\]'
MAGENTA='\[\e[0;35m\]'
BMAGENTA='\[\e[1;35m\]'
BGMAGENTA='\[\e[1;35m\]'
CYAN='\[\e[0;36m\]'
BCYAN='\[\e[1;36m\]'
BGCYAN='\[\e[1;36m\]'
WHITE='\[\e[0;37m\]'
BWHITE='\[\e[1;37m\]'
BGWHITE='\[\e[1;37m\]'

PROMPT_COMMAND=smile_prompt

function smile_prompt
{
		if [ "$?" -eq "0" ]
		then
				#smiley
				SC="${GREEN}$"
		else
				#frowney
				SC="${RED}$"
		fi
		if [ $UID -eq 0 ]
		then
				#root user color
				UC="${RED}"
		else
				#normal user color
				UC="${WHITE}"
		fi
		#hostname color
		HC="${BYELLOW}"
		#regular color
		RC="${BWHITE}"
		#default color
		DF='\[\e[0m\]'
		PS1="[${UC}\u${RC}@${HC}\h ${RC}\W${DF}]${SC}${DF} "
}

export CC="gcc"
export CFLAGS="-ggdb3 -O0 -std=c99 -Wall -Werror"
export LDLIBS="-lm"

PATH="~/bin:$HOME/perl5/bin${PATH+:}${PATH}"; export PATH;
PERL5LIB="$HOME/perl5/lib/perl5${PERL5LIB+:}${PERL5LIB}"; export PERL5LIB;
PERL_LOCAL_LIB_ROOT="$HOME/perl5${PERL_LOCAL_LIB_ROOT+:}${PERL_LOCAL_LIB_ROOT}"; export PERL_LOCAL_LIB_ROOT;
PERL_MB_OPT="--install_base \"$HOME/perl5\""; export PERL_MB_OPT;
PERL_MM_OPT="INSTALL_BASE=$HOME/perl5"; export PERL_MM_OPT;

umask 0002
#ignore duplicates in command history
export HISTCONTROL=ignoredups
export HISTSIZE=750
alias ls='ls --color=auto'
alias ll='ls -l --color=auto'

if [ -f /usr/lib/python3.5/site-packages/powerline/bindings/bash/powerline.sh ]; then
		    source /usr/lib/python3.5/site-packages/powerline/bindings/bash/powerline.sh
fi
#test -r "$HOME/Code/nit/nit/misc/nit_env.sh" && NIT_DIR="$HOME/Code/nit/nit" . "$HOME/Code/nit/nit/misc/nit_env.sh"

# added by travis gem
[ -f $HOME/.travis/travis.sh ] && source /home/alex/.travis/travis.sh
