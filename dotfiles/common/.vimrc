set nocompatible              " required
filetype off                  " required

set encoding=utf-8
set nu "line numbering
set wildmenu "Command completion menu
set lazyredraw "Redraw only when necessary
set noexpandtab "Tabs stay as tabs
set tabstop=4 "Tab width of 4 spaces
" set the runtime path to include Vundle and initialize
set rtp+=~/.vim/bundle/Vundle.vim
call vundle#begin()

" alternatively, pass a path where Vundle should install plugins
"call vundle#begin('~/vim_plugins)

" let Vundle manage Vundle, required
Plugin 'gmarik/Vundle.vim'

" Enable folding with space
set foldmethod=indent
set foldlevel=99
nnoremap <space> za

" Add all your plugins here (note older versions of Vundle used Bundle instead of Plugin)
Bundle 'Valloric/YouCompleteMe'
Plugin 'scrooloose/syntastic'
Plugin 'scrooloose/nerdtree'
Plugin 'jistr/vim-nerdtree-tabs'
Plugin 'ctrlpvim/ctrlp.vim'
Plugin 'tpope/vim-fugitive'
Plugin 'tpope/vim-surround'
Plugin 'tpope/vim-vividchalk'
" Plugin 'jiangmiao/auto-pairs'
Plugin 'vim-scripts/po.vim'
Plugin 'yangmillstheory/vim-snipe'
Plugin 'cakebaker/scss-syntax.vim'
" PlantUML
Plugin 'aklt/plantuml-syntax'
Plugin 'scrooloose/vim-slumlord'
" C++
Plugin 'octol/vim-cpp-enhanced-highlight'
" Python
Bundle "lepture/vim-jinja"
Plugin 'vim-scripts/indentpython.vim'
Plugin 'tweekmonster/django-plus.vim'
" Clojure
Plugin 'tpope/vim-fireplace'
Plugin 'tpope/vim-salve'
Plugin 'kien/rainbow_parentheses.vim'
Plugin 'guns/vim-clojure-static'
Plugin 'guns/vim-clojure-highlight'
Plugin 'guns/vim-sexp'
Plugin 'tpope/vim-sexp-mappings-for-regular-people'
" Haskell
Plugin 'bitc/vim-hdevtools'
Plugin 'neovimhaskell/haskell-vim'
Plugin 'dense-analysis/ale'
" Powershell
Plugin 'PProvost/vim-ps1'

" All of your Plugins must be added before the following line
call vundle#end()            " required
filetype plugin indent on    " required

let python_highlight_all=1
syntax on
set hlsearch
set tabstop=4
set autoindent
set shiftwidth=4
colo vividchalk

" Set executable bit for py files
set autoread
au BufWritePost *.py silent !chmod u+x <afile>

"remove trailing spaces when saving
autocmd BufWritePre * :%s/\s\+$//e

" filenames like *.xml, *.html, *.xhtml, ...
let g:closetag_filenames = "*.html,*.xhtml,*.phtml"
" Auto basic structure for new html file
au BufNewFile *.html 0r ~/.vim/skeleton.html | let IndentStyle = "html"
" Auto basic structure for new c file
au BufNewFile *.c 0r ~/.vim/skeleton.c | let IndentStyle = "c"
" Auto basic structure for new python file
au BufNewFile *.py 0r ~/.vim/skeleton.py | let IndentStyle = "python"

".pro file is Prolog
au BufNewFile,BufRead *.pro set filetype=prolog
au BufNewFile,BufRead *.plt set filetype=prolog

" Move between panes in split mode. CTRL + Vim direction key
nnoremap <C-J> <C-W><C-J>
nnoremap <C-K> <C-W><C-K>
nnoremap <C-L> <C-W><C-L>
nnoremap <C-H> <C-W><C-H>

" open new buffer to the right or below
set splitbelow
set splitright

"Close autocomplete window after use
let g:ycm_path_to_python_interpreter = '/usr/bin/python'
let g:ycm_autoclose_preview_window_after_completion=1
let g:ycm_collect_identifiers_from_tags_files = 1 " Let YCM read tags from Ctags file
let g:ycm_use_ultisnips_completer = 1 " Default 1, just ensure
let g:ycm_seed_identifiers_with_syntax = 1 " Completion for language's keyword
let g:ycm_complete_in_comments = 1 " Completion in comments
let g:ycm_complete_in_strings = 1 " Completion in string

"Syntax highlighting for Nit
let g:syntastic_nit_checkers = ['nitpick']

"ignore buildfiles
let NERDTreeIgnore=['\.pyc$', '\~$']

"python with virtualenv support
py3 << _EOF_
import os
import sys
if 'VIRTUAL_ENV' in os.environ:
	project_base_dir = os.environ['VIRTUAL_ENV']
	activate_this = os.path.join(project_base_dir, 'bin/activate_this.py')
	exec(open(activate_this).read(), dict(__file__=activate_this))
_EOF_

" Powerline
set rtp+=/usr/lib/python3.8/site-packages/powerline/bindings/vim/
set laststatus=2

" Use 256 colours
set t_Co=256
