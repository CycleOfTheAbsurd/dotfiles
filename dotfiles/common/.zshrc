zstyle ':prezto:module:prompt' theme 'agnoster'
plugins=(virtualenv)

export CC="gcc"
export CFLAGS="-ggdb3 -O0 -std=c99 -Wall -Werror"
export LDLIBS="-lm"

PATH="$HOME/bin:$HOME/perl5/bin${PATH+:}${PATH}:$HOME/.local/bin";
PATH="${PATH}:/root/.gem/ruby/2.6.0/bin:$HOME/.gem/ruby/2.6.0/bin"; export PATH;
PERL5LIB="$HOME/perl5/lib/perl5${PERL5LIB+:}${PERL5LIB}"; export PERL5LIB;
PERL_LOCAL_LIB_ROOT="$HOME/perl5${PERL_LOCAL_LIB_ROOT+:}${PERL_LOCAL_LIB_ROOT}"; export PERL_LOCAL_LIB_ROOT;
PERL_MB_OPT="--install_base \"$HOME/perl5\""; export PERL_MB_OPT;
PERL_MM_OPT="INSTALL_BASE=$HOME/perl5"; export PERL_MM_OPT;
export PATH=$PATH:$HOME/.gem/ruby/2.4.0/bin
setopt OCTAL_ZEROES
umask 0002
set +o noclobber
#ignore duplicates in command history
export HISTCONTROL=ignoredups
export HISTSIZE=750
alias ls='ls --color=auto'
alias ll='ls -l --color=auto'
alias xclipb="xclip -selection clipboard"
alias msfconsole="msfconsole --quiet -x \"db_connect ${USER}@msf\""
alias java7="/usr/lib/jvm/java-7-openjdk/bin/java"
alias java8="/usr/lib/jvm/java-8-openjdk/bin/java"
alias ssh-session="eval \"$(ssh-agent -s)\"; ssh-add;"
alias randomman="man \`whatis -r '.' | shuf | head -1 | cut -f1 -d ' '\`"
alias pi-qemu='qemu-system-arm \
  -M versatilepb \
  -cpu arm1176 \
  -m 256 \
  -net nic \
  -net user,hostfwd=tcp::5022-:22 \
  -dtb /run/media/alex/Data/qemu-vms/versatile-pb.dtb \
  -kernel /run/media/alex/Data/qemu-vms/kernel-qemu-4.14.79-stretch \
  -append "root=/dev/sda2 panic=1" \
  -no-reboot \
  -hda /run/media/alex/Data/qemu-vms/raspbian.img
'
alias ghc="ghc -dynamic"

#Base64 Decode/Encode a string
function b64d() {
	echo -n "$1" | base64 -d; echo
}
function b64e() {
	echo -n "$1" | base64;
}

source /usr/share/zsh/plugins/zsh-syntax-highlighting/zsh-syntax-highlighting.zsh

if [[ -r /usr/lib/python3.8/site-packages/powerline/bindings/zsh/powerline.sh ]]; then
	source /usr/lib/python3.8/site-packages/powerline/bindings/zsh/powerline.zsh
fi
prompt agnoster
export VIRTUAL_ENV_DISABLE_PROMPT=0

#Nit
#test -r "$HOME/Code/nit/nit/misc/nit_env.sh" && NIT_DIR="$HOME/Code/nit/nit" . "$HOME/Code/nit/nit/misc/nit_env.sh"

# added by travis gem
[ -f $HOME/.travis/travis.sh ] && source /home/alex/.travis/travis.sh
