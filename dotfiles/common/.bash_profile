#
# ~/.bash_profile
#

[[ -f ~/.bashrc ]] && . ~/.bashrc

if [ -f ~/.bash_git ]; then
    . ~/.bash_git
fi

export EDITOR=vim
export PS1='\u@\h:\w \$(parse_git_branch) $ '
xset -b
